import org.jfrog.gradle.plugin.artifactory.dsl.PublisherConfig
import org.jfrog.gradle.plugin.artifactory.dsl.ResolverConfig
import org.jfrog.gradle.plugin.artifactory.task.ArtifactoryTask

import groovy.lang.GroovyObject

plugins {
    java
    `maven-publish`
    id("com.jfrog.artifactory") version "4.1.1"
}

/*

# Groovy version

artifactory {
    contextUrl = 'https://repo.gradle.org/gradle'
    publish {
        repository {
            repoKey = targetRepoKey
            username = project.findProperty('artifactory_user') ?: 'nouser'
            password = project.findProperty('artifactory_password') ?: 'nopass'
            maven = true
        }
        defaults {
            publications('mavenJava')
        }
    }
    resolve {
        repoKey = 'repo'
    }
}

*/

artifactory {
    setContextUrl("https://repo.gradle.org/gradle")
    publish(delegateClosureOf<PublisherConfig> {
        repository(delegateClosureOf<GroovyObject> {
            val targetRepoKey = "libs-milestones-local"
            setProperty("repoKey", targetRepoKey)
            setProperty("username", project.findProperty("artifactory_user") ?: "nouser")
            setProperty("password", project.findProperty("artifactory_password") ?: "nopass")
            setProperty("maven", true)
        })
        defaults(delegateClosureOf<GroovyObject> {
            invokeMethod("publications", "mavenJava")
        })
    })
    resolve(delegateClosureOf<ResolverConfig> {
        setProperty("repoKey", "repo")
    })
}

publishing {
    (publications) {
        "mavenJava"(MavenPublication::class) {
            from(components["java"])
        }
    }
}
